import Row from "react-bootstrap/Row";
import {Col, Container} from "react-bootstrap";
import './Contacts.css';

function Contacts() {

    return (
        <div className="Contacts">
            <Container>
                <Row className="RowItem">
                    <Col xs={6} md={4}>Телефон</Col>
                    <Col xs={12} md={8}>8-800-555-35-35</Col>
                </Row>
                <Row className="RowItem">
                    <Col xs={6} md={4}>E-mail</Col>
                    <Col xs={12} md={8}>homebakery@gmail.com</Col>
                </Row>
                <Row className="RowItem">
                    <Col xs={6} md={4}>Юр. адрес</Col>
                    <Col xs={12} md={8}>г. Новосибирск, ул. Красный Проспект, 1, 630000</Col>
                </Row>
                <Row className="RowItem">
                    <Col xs={6} md={4}>Оплата</Col>
                    <Col xs={12} md={8}>Наличными, банковской картой, переводом на карту</Col>
                </Row>
            </Container>
        </div>
    );
}

export default Contacts;