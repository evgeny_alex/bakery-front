import axios from 'axios';
import {useEffect, useState} from "react";
import CatalogItem from "./CatalogItem";
import './Catalog.css';
import {Container} from "react-bootstrap";
import Row from 'react-bootstrap/Row'

function Catalog(props) {

    const [dishList, setDishList] = useState([]);

    useEffect(() => {
        const apiUrl = 'http://localhost:8080/dish/list';
        axios.get(apiUrl).then((resp) => {
            const data = resp.data;
            setDishList(data);
        });
    }, [setDishList]);

    return (
        <div>
            <Container>
                <Row lg={3} md={3} sm={3} xl={3} xs={3}>
                    {dishList.map(item =>
                        <CatalogItem key={item.id} id={item.id} label={item.label}
                                     description={item.description}
                                     cost={item.cost} basket={props.basket} setBasket={props.setBasket}
                                     basketCount={props.basketCount} setBasketCount={props.setBasketCount}/>)
                    }
                </Row>

            </Container>
        </div>
    );
}

export default Catalog;