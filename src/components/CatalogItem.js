import belyash from '../resources/image/belyash.png';
import cheburek from '../resources/image/cheburek.png';
import cherry from '../resources/image/cherry.png';
import chicken from '../resources/image/chicken.png';
import pizza from '../resources/image/pizza.png';
import img from '../resources/image/img.png';
import Button from "react-bootstrap/cjs/Button";
import './Catalog.css';

function CatalogItem(props) {

    const addToBasket = () => {
        let listBasket = props.basket;
        listBasket.push({
            id: props.id,
            label: props.label,
            description: props.description,
            cost: props.cost
        })
        props.setBasket(listBasket)
        props.setBasketCount(props.basketCount + 1)
        console.log(props.basket)
    }

    const getImageById = (id) => {
        switch (id) {
            case 1:
                return pizza;
            case 2:
                return chicken;
            case 3:
                return cherry;
            case 4:
                return belyash;
            case 5:
                return cheburek;
            default:
                return img;
        }
    }

    return (
        <div className="catalogItem">
            <img alt="" width="170" height="120" src={getImageById(props.id)}/>
            <p>{props.label}</p>
            <p>{props.description}</p>
            <p>Цена: {props.cost}</p>
            <Button variant="success" onClick={addToBasket}>Добавить в корзину</Button>
        </div>
    );
}

export default CatalogItem;