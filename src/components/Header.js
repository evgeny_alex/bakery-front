import './Header.css';
import {Container, Form, Nav, Navbar} from "react-bootstrap";
import Button from "react-bootstrap/cjs/Button";
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Switch from "react-bootstrap/Switch";
import Catalog from "./Catalog";
import Map from "./Map";
import ModalOrder from "./ModalOrder";
import {useState} from "react";
import Contacts from "./Contacts";

function Header() {

    function withProps(Component, props) {
        return function(matchProps) {
            return <Component {...props} {...matchProps} />
        }
    }

    const [modalShow, setModalShow] = useState(false);

    const [basket, setBasket] = useState([]);

    const [basketCount, setBasketCount] = useState(0)

    const createOrder = (e) => {
        setModalShow(true);
        return undefined;
    }
    return (
        <>
            <Navbar bg="light" variant="light">
                <Container>
                    <Navbar.Brand href="/">Домашняя пекарня</Navbar.Brand>
                    <Nav className="me-auto">
                        <Nav.Link href="/">Каталог</Nav.Link>
                        <Nav.Link href="/map">Карта</Nav.Link>
                        <Nav.Link href="/contacts">Контакты</Nav.Link>
                    </Nav>
                    <Form inline>
                        <Navbar.Text className="basketCount">
                            Корзина: {basketCount}
                        </Navbar.Text>
                        <Button variant="outline-info" onClick={(e) => createOrder(e)}>Оформить заказ</Button>
                    </Form>
                </Container>
            </Navbar>

            <Router>
                <Switch>
                    <Route exact path="/" component={withProps(Catalog, { basket: basket, setBasket: setBasket,
                        basketCount: basketCount, setBasketCount: setBasketCount })} />
                    <Route exact path="/map" component={Map}/>
                    <Route exact path="/contacts" component={Contacts}/>
                </Switch>
            </Router>

            <ModalOrder show={modalShow}
                        onHide={() => setModalShow(false)}
                        basket={basket}/>
        </>
    );
}

export default Header;