import map from '../resources/image/map.png';
import './Map.css';

function Map() {

    return (
        <div className='Map'>
            <img alt="" width={300} height={320} src={map}/>
            <div>Зеленая зона. При сумме заказа менее 500р, доставка 70 р.</div>
            <div>Синяя зона. При сумме заказа менее 700р, доставка 100 р.</div>
            <div>Оранженая зона. При сумме заказа менее 1500р, доставка 300 р.</div>
            <div>Красная зона. При сумме заказа менее 2000р, доставка 350 р.</div>
        </div>
    );
}

export default Map;