import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button';
import {Col, Container} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Form from 'react-bootstrap/Form'
import './ModalOrder.css'
import axios from "axios";
import {useState} from "react";

function ModalOrder(props) {

    const [phone, setPhone] = useState('')

    const [address, setAddress] = useState('')

    const [orderDate, setOrderDate] = useState('')

    const getSum = (basket) => {
        let result = 0;
        basket.forEach(item => {
            result += parseInt(item.cost);
        })
        return result;
    }

    const getOrderComposition = (basket) => {
        let mapCount = new Map();
        basket.forEach(item => {
            if (mapCount.has(item.label)) {
                mapCount.set(item.label, String(parseInt(mapCount.get(item.label)) + 1));
            } else {
                mapCount.set(item.label, '1');
            }
        })
        let result = ''
        mapCount.forEach((value, key) => {
            result += key + ': ' + value + '; '
        });
        return result
    }

    const sendOrder = (basket) => {
        const data = {
            basket: basket,
            orderDate: orderDate,
            phone: phone,
            address: address,
            sum: getSum(basket)
        }
        axios.post('http://localhost:8080/ordering/create', data,
            {
                headers: {
                    'Access-Control-Allow-Origin': '*'
                }
            })
            .then(() => {})
    }

    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">
                    Параметры заказа
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Container>
                    <Row>
                        <Col xs={10} md={10}>
                            Состав заказа: {getOrderComposition(props.basket)}
                        </Col>
                    </Row>
                    <Row className="FormRow">
                        <Col xs={10} md={10}>
                            Сумма заказа: {getSum(props.basket)}
                        </Col>
                    </Row>
                    <Row className="FormRow">
                        <Col xs={6} md={4}>
                            Номер телефона
                        </Col>
                        <Col xs={6} md={6}>
                            <Form.Control onChange={event => setPhone(event.target.value)} type="text" placeholder="Номер телефона"/>
                        </Col>
                    </Row>
                    <Row className="FormRow">
                        <Col xs={6} md={4}>
                            Дата доставки
                        </Col>
                        <Col xs={6} md={6}>
                            <input type="date" onChange={event => setOrderDate(event.target.value)}
                                   className="form-control" id="date" name="date"
                                   placeholder="Дата" required/>
                        </Col>
                    </Row>
                    <Row className="FormRow">
                        <Col xs={6} md={4}>
                            Адрес
                        </Col>
                        <Col xs={6} md={6}>
                            <Form.Control type="text" onChange={event => setAddress(event.target.value)}
                                          placeholder="Адрес"/>
                        </Col>
                    </Row>
                </Container>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={() => {
                    sendOrder(props.basket)
                    props.onHide()
                }}>
                    Заказать
                </Button>
                <Button onClick={props.onHide}>Закрыть</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default ModalOrder;